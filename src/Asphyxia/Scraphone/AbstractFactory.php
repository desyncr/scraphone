<?php
/*
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 * 
 * @copyright Copyleft
 */
namespace Asphyxia\Scraphone;

/**
 * Handles the creation of classes
 *
 * @author asphxr
 */
class AbstractFactory {

    /**
     * Returns an instance of \Asphyxia\Scraphone\[$namespace\]$type class
     * 
     * @param String $type
     * @param String $namespace
     * @return \Asphyxia\Scraphone\class
     * @throws \Exception
     */
    public static function build($type, $namespace = '') {
        $class = "Asphyxia\Scraphone\\${namespace}\\${type}";

        if (class_exists($class)) {
            return new $class;
        }else{
            throw new \Exception('AbstractFactory : class not found: ' . $class);
        }
    }
}