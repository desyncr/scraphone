<?php
/*
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 * 
 * @copyright Copyleft
 */
namespace Asphyxia\Scraphone\Parser;

/**
 * Description of ParserFactory
 *
 * @author asphxr
 */
class ParserFactory extends \Asphyxia\Scraphone\AbstractFactory {
    
    /**
     * Creates a new intance of Parser\Driver\$type class
     * 
     * @param String $type
     * @param Array $config
     * @return \Asphyxia\Scraphone\Parser\Parser
     */
    public static function build($type, $config = null) {
        
        $driver = parent::build($type, "Parser\Drivers");

        $parser = new Parser($driver);
        $parser->bootstrap($config);
        return $parser;

    }
}