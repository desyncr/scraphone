<?php
/*
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 * 
 * @copyright Copyleft
 */
namespace Asphyxia\Scraphone\Parser;
use Asphyxia\Scraphone\Memoize,
    Asphyxia\Coil\Coil;
use Sunra\PhpSimple\HtmlDomParser;
use Sabberworm\CSS\Parser as CssParser;

/**
 * Description of MemoizeParser
 *
 * @author asphxr
 */
class MemoizeParser {
    private $memoize_key_prefix = 'scraphone_';
    private $cssParserMemoizeKey = 'cssparser';
    
    /**
     * Sets the MemoizeParser memoize driver
     * 
     * @param Memoize\Memoize|null $driver
     */
    public function setMemoizer(Memoize\Memoize $driver) {
        $this->m = $driver;
    }

    /**
     * Returns a DOM Object of the $document HTML
     * 
     * @param String $document
     * @return DOM Object
     */
    public function parseDocument($document) {
        return $this->memoize(function($document) {
                    return HtmlDomParser::str_get_html($document);
                }, $document);
    }
    
    /**
     * Retrieves a resource from the web
     * 
     * @param String $url
     * @return String HTML
     */
    public function fetchResource($url) {
        return $this->memoize(function ($url) {
                    return Coil::get($url);
                }, $url, $url);
    }

    /**
     * Retrieves and parses the CSS stylesheet
     * 
     * @param String $stylesheet URL to the stylesheet
     * @return CSSOM
     */
    protected function getRuleSheet($stylesheet){
     
        // Get the stylesheet
        $css = $this->memoize(function($stylesheet) {

            $cssParser = new CssParser($this->fetchResource($stylesheet));
            return $cssParser->parse();

        }, $stylesheet, $this->cssParserMemoizeKey);

        // Get the rulesheets object
        $selector = $this->memoize(function($rules) {
            return $rules->getAllRuleSets();
        }, $css);

        return $selector;
    }
    
    /**
     * Creates a memoization key
     * 
     * @param String $str
     * @return String
     */
    protected function getMemoizeKey($str) {
        if (empty($str) || is_array($str)) {
            throw new \Exception('MemoizeParser: BAD_MEMOIZE_KEY: ' . print_r($str, 1));
        }
        return $this->memoize_key_prefix . md5($str);
    }
    
    /**
     * Memoizes parser resources
     * 
     * @param callable $callback
     * @param Variant $resource
     * @param String|null $key
     * @return Variant
     */
    protected function memoize($callback, $resource, $key = null) {
        $key = $key == null ? $resource : $key;
        $resource = is_array($resource) ? $resource : array($resource);
        return isset($this->m) ? $this->m->memoize($callback, $resource, $this->getMemoizeKey($key)) : call_user_func_array($callback, $resource);
    }
    
}
