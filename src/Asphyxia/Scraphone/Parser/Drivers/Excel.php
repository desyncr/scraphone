<?php
/*
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 * 
 * @copyright Copyleft
 */
namespace Asphyxia\Scraphone\Parser\Drivers;
use Asphyxia\Scraphone\Parser\MemoizeParser;

/**
 * Description of Excel
 *
 * @author asphxr
 */
class Excel extends MemoizeParser implements ParserInterface {
    private $arrFileList = array();
    private $idFileList = 'shLink';
    private $patternRelativePath = '/^(.*\/)/';
    private $defaultStylesheet = 'stylesheet.css';
    private $arrDom = array();

    /**
     * Bootstrap Parser configuration
     * 
     * @param Array $config
     */
    public function bootstrap($config) {
        // stub
    }

    /**
     * Parse a document at the URL $url
     *
     * @param String $url
     * @param DOM Object|null $dom
     */
    public function parse($url) {
        $this->url = $url;
        $this->dom = $this->parseDocument(
                            $this->fetchResource($this->url)
                    );

        // document relative path
        $this->relativePath = $this->getRelativePath($url);
      
        // Get the file list xml
        $this->arrFileList = $this->getRelatedResources();
        if (empty($this->arrFileList)) {
          array_push($this->arrFileList, $url);
        }

        // get stylesheet - heuristically - so to say
        $this->stylesheet = $this->getRelativePath($this->arrFileList[0]);
        $this->stylesheet .= DIRECTORY_SEPARATOR . $this->defaultStylesheet;
        $this->selector = $this->getRuleSheet($this->stylesheet);

        // process them one at a time
        foreach ($this->arrFileList as $sheet) {
          array_push($this->arrDom, $this->parseDocument($this->fetchResource($sheet)));
        }

        return array($this->arrDom, $this->selector);
    }
    
    /**
     * Perform a ParserApi query on the given element
     * 
     * @param String $query Class selector
     * @param type $element Dom element
     */
    public function api($query, $element){
        // stub
    }
    
    /**
     * Get all related documents URLs to the current sheet
     * 
     * @return array
     */
    private function getRelatedResources() {
        $arrFiles = array();

        foreach ($this->dom->find('link') as $link){
            if ($link->__get('id') == $this->idFileList && $link->__get('href') != '') {
                array_push($arrFiles, $this->relativePath . DIRECTORY_SEPARATOR . $link->__get('href'));
            }
        }
        return $arrFiles;
    }

    /**
     * Returns the relative path to the current document $url
     * 
     * @param String $url
     * @return String
     */
    private function getRelativePath($url) {
        $arrMatches = array();
        if (preg_match($this->patternRelativePath, $url, $arrMatches)){
            $relativePath = $arrMatches[0];    
        }else{
            $relativePath = $url;
        }
        return $relativePath;
    }
}