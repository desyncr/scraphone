<?php
/*
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 * 
 * @copyright Copyleft
 */
namespace Asphyxia\Scraphone\Parser\Drivers;

/**
 * Description of ParserInterface
 *
 * @author asphxr
 */
interface ParserInterface {
    
    /**
     * Bootstrap Parser configuration
     * 
     * @param Array $config
     */
    public function bootstrap($config);
    
    /**
     * Parse a document at the URL $url
     * 
     * @param String $url
     */
    public function parse($url);
        
    /**
     * Perform a ParserApi query on the given element
     * 
     * @param String $query Class selector
     * @param type $element Dom element
     */
    public function api($query, $element);
}