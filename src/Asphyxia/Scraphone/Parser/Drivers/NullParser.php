<?php
/*
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 * 
 * @copyright Copyleft
 */
namespace Asphyxia\Scraphone\Parser\Drivers;

/**
 * Description of NullParser
 *
 * @author asphxr
 */
class NullParser implements ParserInterface {

    /**
     * Bootstrap Parser configuration
     * 
     * @param Array $config
     */
    public function bootstrap($config) {
        // stub
    }

    /**
     * Parse a document at the URL $url
     *
     * @param String $url
     * @param DOM Object|null $dom
     */
    public function parse($url) {
        // stub
    }
    
    /**
     * Perform a ParserApi query on the given element
     * 
     * @param String $query Class selector
     * @param type $element Dom element
     */
    public function api($query, $element){
        // stub
    }
}