<?php
/*
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 * 
 * @copyright Copyleft
 */
namespace Asphyxia\Scraphone\Parser\Drivers;

/**
 * 
 */
class ExcelApi {
    
    /**
     * 
     * @param type $api
     */
    public function __construct($api) {
        $this->api = $api;
    }
    
    /**
     * 
     * @param type $row
     * @param type $col
     * @return type
     */
    public function moveTo($row, $col = 0) {

        $this->api->rollback();

        $element = $this->api->api('tbody tr')->getElement($row);

        $this->api->parse($element->innertext);

        $subelement = $this->api->api('td')->getElement($col);
        
        return $subelement;
    }
}