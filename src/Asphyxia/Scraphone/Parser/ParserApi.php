<?php
/*
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 * 
 * @copyright Copyleft
 */
namespace Asphyxia\Scraphone\Parser;
use Asphyxia\Scraphone\Parser;

/**
 * Wraps the API interface to Parser
 */
class ParserApi {
    
    /**
     * 
     * @param type $dom
     */
    public function __construct($dom) {
        $this->dom = $dom;
        $this->current_element = $dom;
    }

    /**
     * Search the previous sibling of $element
     * 
     * @param DOM Object $element
     * @return DOM Object
     */
    public function prev_sibling(){
        return $this->current_element = $this->current_element->prev_sibling();
    }
    
    /**
     * Get the inner-text of $element
     * 
     * @param DOM Object $element
     * @return String
     */
    public function innertext() {
        return $this->current_element->innertext;
    }
    
    /**
     * Wraps functions around Parser API
     * 
     * @param String $selector
     * @param DOM Object $element
     * @return Variant
     */
    public function api($selector) {
        switch ($selector) {
            case '::next-sibling':
                return $this->next_sibling();
                break;
            case '::prev-sibling':
                return $this->prev_sibling();
                break;
            default:
                $this->current_element = $this->current_element->findDomElement($selector);
                return $this;
        }
    }
    
    public function getElement($at = 0) {
        return $this->current_element[$at];
    }
    
    public function getElements() {
        return $this->current_element;
    }
    
    public function each($callback) {
        foreach($this->current_element as $element) {
            call_user_func($callback, $element);
        }
    }
    
    public function rollback(){
        $this->current_element = $this->dom;
    }
    
    public function parse($html) {
        $parser = new Parser\Parser(new Parser\Drivers\NullParser());
        $parser->parse(false, $html);
        return $this->current_element = $parser;
    }
}
