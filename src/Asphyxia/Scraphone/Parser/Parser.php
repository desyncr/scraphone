<?php
/*
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 * 
 * @copyright Copyleft
 */
namespace Asphyxia\Scraphone\Parser;
use Asphyxia\Scraphone\Memoize;
use Sunra\PhpSimple\HtmlDomParser;

/**
 * Description of Parser
 *
 * @author asphxr
 */
class Parser extends MemoizeParser implements Drivers\ParserInterface {
    private $driver;

    /**
     * Creates a new instance of Parser
     * 
     * @param Object $driver
     */
    public function __construct(Drivers\ParserInterface $driver, $api = null) {
        $this->driver = $driver;
        $this->api = $api;
    }
    
    /**
     * Bootstrap Parser configuration
     * 
     * @param Array $config
     */
    public function bootstrap($config) {
        $memoizeDriver = $config['memoize']['driver'];
        $memoizeConfig = $config['memoize'][strtolower($memoizeDriver)];
        $m = Memoize\MemoizeFactory::build($memoizeDriver, $memoizeConfig);
        $this->setMemoizer($m);

        $this->driver->bootstrap($config);
        // Forcer driver to use the same memoize instance
        $this->driver->setMemoizer($m);
    }
    
    /**
     * Parse a document at the URL $url
     *
     * @param String $url
     * @param DOM Object|null $dom
     */
    public function parse($url, $strHtml = null) {

        $this->url = $url;
        
        // PHP Fatal error:  Asphyxia\Scraphone\Parser\Drivers\Excel::parse(): 
        // The script tried to execute a method or access a property of an incomplete 
        // object. Please ensure that the class definition "simple_html_dom" of the 
        // object you are trying to operate on was loaded _before_ unserialize() 
        // gets called or provide a __autoload() function to load the class definition  
        // in /home/asphxr/Workspace/ringcaptcha/Scraphone/src/Asphyxia/Scraphone/Parser/Drivers/Excel.php on line 18
        HtmlDomParser::str_get_html('');

        if ($strHtml) {
            return $this->arrDom = array(HtmlDomParser::str_get_html($strHtml));
        }
        // oh, god, no
        list($this->arrDom, $this->selector) = $this->driver->parse($this->url);
    }
    
    /**
     * Find a DOM element of class $element
     * 
     * @param String $element Class selector
     * @param callable $callback WARNING: Not called for each element
     */
    public function findDomElement($element, $callback = null) {
        
        $arrReturn = $this->memoize(function($element, $arrDom) {

            $arrReturn = array();
            foreach ($arrDom as $dom) {
                $arrReturn = array_merge($arrReturn, $dom->find($element));
            }
            return $arrReturn;
            
        }, array($element, $this->arrDom), $element);

        if ($callback) $arrReturn = call_user_func($callback, $arrReturn);
        return $arrReturn;
    }

    /**
     * Find a related CSS declaration block (rules) and return it's property $property
     * 
     * @param String $element Class selector
     * @param String $property Property name
     * @param callable $callback WARNING: Not called for each element
     */
    public function findCssDeclaration($element, $property = null, $callback = null) {
        $arrReturn = $this->memoize(function($element, $property = null) {

            $arrReturn = array();
            foreach ($this->selector as $selector) {
                if (in_array(".${element}", $selector->getSelectors())) {
                    if ($property) {
                        return $this->getCssProperty($selector, $property);
                    }else{
                        array_push($arrReturn, $selector);
                    }
                }
            }

        }, array($element, $property), "${element}${property}");

        if ($callback) $arrReturn = call_user_func($callback, $selector);
        return $arrReturn;
    }
    
    /**
     * Returns the CSS $property property (rule) of selector $selector
     * 
     * @param String $selector Class selector
     * @param String $property Property name
     * @param Boolean $toString Forces string convertion
     */
    public function getCssProperty($selector, $property, $toString = true) {
        if ((false != $rules = $selector->getRules($property)) && !empty($rules)) {
            $propertyValue = $rules[0]->getValue();
            if ($toString && is_object($propertyValue)) {
                $propertyValue = $propertyValue->__toString();
            }
            return $propertyValue;
        }
        return false;
    } 
    
    /**
     * Returns all CSS selectors
     */
    public function getAllCssSelectors() {
        return $this->selector;
    }
    
    /**
     * Perform a ParserApi query on the given element
     * 
     * @param String $query Class selector
     * @param type $element Dom element
     * @return Variant
     */
    public function api($query, $element) {
        return $this->api ? $this->api->api($query, $element) : null;
    }
}