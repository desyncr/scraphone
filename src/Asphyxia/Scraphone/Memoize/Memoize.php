<?php
/*
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 * 
 * @copyright Copyleft
 */
namespace Asphyxia\Scraphone\Memoize;

/**
 * Description of Memoize
 *
 * @author asphxr
 */
class Memoize {
    private $driver;
    private $key;

    /**
     * Creates a new instance of Memoize
     * 
     * @param Memoize\Drivers\MemoizeInterface $driver
     */
    public function __construct(Drivers\MemoizeInterface $driver) {
        $this->driver = $driver;
    }
    
    /**
     * Bootstrap the Memoizer
     * 
     * @param Array $config
     * @return type
     */
    public function bootstrap($config = null) {
        $this->setDebug($config['debug']);
        $this->driver->bootstrap($config);
    }
    
    /**
     * Sets Memoizer key
     * 
     * @param String $key
     */
    public function setKey($key) {
        $this->key = $key;
    }
    
    /**
     * Enables Memoizer debug output
     * 
     * @param Boolean $enable
     */
    public function setDebug($enable = false) {
        $this->driver->debug = ($enable == true);
    }
    
    /**
     * Memoizes objects
     * 
     * @param callable $callback
     * @param Array $params
     * @param String $key
     * @return Variant
     */
    public function memoize($callback, $params, $key = null) {
        $key = $key == null ? $this->key : $key;

        if (false == $content = $this->driver->memoize($key)) {
            return $this->driver->memoize($key, call_user_func_array($callback, $params));
        }else{
            return $content;
        }
    }
}