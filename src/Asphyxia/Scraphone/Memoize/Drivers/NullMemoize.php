<?php
/*
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 * 
 * @copyright Copyleft
 */
namespace Asphyxia\Scraphone\Memoize\Drivers;

/**
 * Description of FileSystem
 *
 * @author asphxr
 */
class NullMemoize implements MemoizeInterface {

    /**
     * Bootstrap Memoize configuration
     * 
     * @param Array $config
     */
    public function bootstrap($config){
        // stub
    }
    
    public $debug = false;
    /**
     * Memoizes an object
     * 
     * @param String $key
     * @param Variant $data
     * @return Variant
     */
    public function memoize($key, $data = null) {
        if ($this->debug) echo 'MEMOIZE::NULL_MEMOIZE - ' . $key . ' - ' . $data . PHP_EOL;
        return $data == null ? false : $data;
    }
}