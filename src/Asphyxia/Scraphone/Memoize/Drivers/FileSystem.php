<?php
/*
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 * 
 * @copyright Copyleft
 */
namespace Asphyxia\Scraphone\Memoize\Drivers;

/**
 * Description of FileSystem
 *
 * @author asphxr
 */
class Filesystem implements MemoizeInterface {
    private $path = '/tmp/';
    private $prefix = '.memoize-';
    public $debug = false;

    /**
     * Bootstrap Memoize configuration
     * 
     * @param Array $config
     */
    public function bootstrap($config) {
        // stub
    }
    
    /**
     * Memoizes an object to disk
     * 
     * @param String $key
     * @param Variant $data
     * @return Variant
     */
    public function memoize($key, $data = null) {
        if ($this->debug) {'MEMOIZE::FILESYSTEM_MEMOIZE - ' . $key . PHP_EOL;}
        $memoize_file = $this->path . DIRECTORY_SEPARATOR . $this->prefix . $key;
        
        if ($data == null) {
            return $this->getMemoize($memoize_file);
        }

        file_put_contents($memoize_file, serialize($data));
        return $data;

    }
    
    /**
     * Returns false is no memoize object found, the object otherwise
     * 
     * @param String $key
     * @return Variant|false
     */
    private function getMemoize($key) {
        if (file_exists($key)){
            if ($this->debug) {echo 'Memoize::FROM_MEMORY : ' . $key .PHP_EOL;}
            return unserialize(file_get_contents($key));
        }else{
            if ($this->debug) {echo 'Memoize::ERROR_NO_MEMOIZED : ' . $key .PHP_EOL;}
            return false;
        }
    }
}