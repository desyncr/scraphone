<?php
/*
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 * 
 * @copyright Copyleft
 */
namespace Asphyxia\Scraphone\Memoize\Drivers;

/**
 * Description of MemoizeInterface
 *
 * @author asphxr
 */
interface MemoizeInterface {
    /**
     * Bootstrap Memoize configuration
     * 
     * @param Array $config
     */
    public function bootstrap($config);

    /**
     * Memoizes an object
     * 
     * @param String $key
     * @param Variant $data
     * @return Variant
     */
    public function memoize($key, $data = null);
}