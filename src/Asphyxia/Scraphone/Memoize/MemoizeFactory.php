<?php
/*
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 * 
 * @copyright Copyleft
 */
namespace Asphyxia\Scraphone\Memoize;

/**
 * Description of MemoizeFactory
 *
 * @author asphxr
 */
class MemoizeFactory extends \Asphyxia\Scraphone\AbstractFactory {
    
    /**
     * Creates a new intance of Memoize\Driver\$type class
     * 
     * @param String $type
     * @param Array $config
     * @return \Asphyxia\Scraphone\Memoize\Memoize
     */
    public static function build($type, $config = null) {
        $driver = parent::build($type, "Memoize\Drivers");
        
        $memoize = new Memoize($driver);
        $memoize->bootstrap($config);
        return $memoize;

    }
}